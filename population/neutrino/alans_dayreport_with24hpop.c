/**************************************************************************
*
* File:		$RCSfile$
* Module:	
* Part of:	
*
* Revision:	$Revision$
* Last edited:	$Date$
* Author:	$Author$
* Copyright:	(c) 2015 Reveal Limited
*
* Notes:	
* Private func:	
* History:	
*
**************************************************************************/

/* INCLUDES =============================================================*/

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>

#include <anInterface.h>

#include <ctr_spec.h>

#include "day_data.h"

/* PRIVATE DEFINES ======================================================*/

#define SOD "00:00"

#define UPPER 60.0
#define LOWER  6.0

#define ALPHA 0.10
#define BETA  0.02

/* PRIVATE TYPES ========================================================*/
/* ADDITIONAL DECLARATIONS ==============================================*/
/* PRIVATE FORWARD DECLARATIONS =========================================*/
/* PUBLIC VARIABLES =====================================================*/

/* PRIVATE VARIABLES ====================================================*/
/* PRIVATE FUNCTIONS ====================================================*/

/**************************************************************************
*
* Function:	printUsage
* Description:	Prints usage to stderr
* Arguments:	
* Returns:	
* Notes:	
*
**************************************************************************/
static void printUsage(void)
{
    fprintf(stderr, "Usage: dayreport_with24hpop [-i sid:zid:cid]* [-o sid:zid:cid]* [-I cidspecfile]* -[O cidspecfile]* -w when -r rawobs [-S HH:MM] [-s pop] [-f pop] [-b size] [-u upper] [-l lower] [-t trend]\n");
    fprintf(stderr, "\nWhere required arguments are:\n");
    fprintf(stderr, "\t-i sid:zid:cid = in counter spec\n");
    fprintf(stderr, "\t-o sid:zid:cid = out counter spec\n");
    fprintf(stderr, "\t-I cidspecfile = file containing in counter specs, one per line\n");
    fprintf(stderr, "\t-O cidspecfile = file containing out counter specs, one per line\n");
    fprintf(stderr, "\t-w when        = day to process (YYYY-MM-DD)\n");
    fprintf(stderr, "\t-r rawobs      = rawobs file\n");
    fprintf(stderr, "\nand optional arguments are:\n");
    fprintf(stderr, "\t-S HH:MM       = start of day (%s)\n", SOD);
    fprintf(stderr, "\t-b binsize     = bin size in seconds (60)\n");
    fprintf(stderr, "\t-s pop         = start of day population (%.1f)\n", LOWER);
    fprintf(stderr, "\t-f pop         = finish of day population (same as start)\n");
    fprintf(stderr, "\t-u upper       = upper bound (%.1f)\n", UPPER);
    fprintf(stderr, "\t-l lower       = lower bound (%.1f)\n", LOWER);
    fprintf(stderr, "\t-t trend       = trend to remove in people/day (0.0)\n");
    fprintf(stderr, "Defaults are shown in ()\n");
    fprintf(stderr, "\ndayreport_with24hpop - version: %s\n\n", pmReleaseName() );
}


/* PUBLIC FUNCTIONS =====================================================*/

/**************************************************************************
*
* Notes:	
*
**************************************************************************/
int main(int argc, char ** argv)
{
    int opt;
    int rc;
    CTR_SPEC_LIST * in_specs;
    CTR_SPEC_LIST * out_specs;
    char * when_string;
    char * countFileName;
    unsigned int ** in_data;
    unsigned int ** out_data;
    unsigned int numbins;
    unsigned int idx;
    unsigned int binsize_sec;
    unsigned int sidx;
    unsigned int row_total;
    unsigned int col_total;
    char buf[4096];
    int population;
    int corrected_population;
    int corrected_population2;
    double upper_b;
    double lower_b;
    double upper;
    double lower;
    unsigned int bin_b;
    unsigned int bin_c;
    unsigned int in_b;
    unsigned int out_b;
    struct tm tm;
    struct timeval sdate;
    struct timeval fdate;
    char * sod_string;
    double start_hour;
    double start;
    double finish;
    double error_bound;
    double trend;
    double trend_per_bin;
    double corrected_population3;
    double corrected_population4;
	    double alpha;
	    double beta;
	    double lambda;
    
    /* load some defaults for the arguments */
    when_string = NULL;
    in_specs = allocCtrSpecList();
    if ( in_specs == NULL )
    {
	fprintf(stderr, "allocCtrSpecList() returned NULL\n");
	exit(1);
    }
    out_specs = allocCtrSpecList();
    if ( out_specs == NULL )
    {
	fprintf(stderr, "allocCtrSpecList() returned NULL\n");
	exit(1);
    }
    countFileName = NULL;
    binsize_sec = 60;
    start = LOWER;
    upper = UPPER;
    lower = LOWER;
    sod_string = SOD;
    finish = -1.0;
    trend = 0.0;

    /* get the command line arguments */
    while ( ( opt = getopt(argc, argv, "i:o:I:O:w:r:b:s:f:u:l:S:t:Uh") ) != -1 )
    {
	switch ( opt )
	{
	    case 'i':
		rc = ctrSpecParseString(in_specs, optarg);
		if ( rc != PM_RV_OK )
		{
		    fprintf(stderr, "ctrSpecParseString() returned %d\n", rc);
		    exit(1);
		}
		break;

	    case 'o':
		rc = ctrSpecParseString(out_specs, optarg);
		if ( rc != PM_RV_OK )
		{
		    fprintf(stderr, "ctrSpecParseString() returned %d\n", rc);
		    exit(1);
		}
		break;

	    case 'I':
		rc = ctrSpecParseFile(in_specs, optarg);
		if ( rc != PM_RV_OK )
		{
		    fprintf(stderr, "ctrSpecParseFile() returned %d\n", rc);
		    exit(1);
		}
		break;

	    case 'O':
		rc = ctrSpecParseFile(out_specs, optarg);
		if ( rc != PM_RV_OK )
		{
		    fprintf(stderr, "ctrSpecParseFile() returned %d\n", rc);
		    exit(1);
		}
		break;

	    case 'w':
		when_string = optarg;
		break;

	    case 'r':
		countFileName = optarg;
		break;

	    case 'b':
		binsize_sec = atoi(optarg);
		break;

	    case 's':
		start = atof(optarg);
		break;

	    case 'f':
		finish = atof(optarg);
		break;

	    case 'u':
		upper = atof(optarg);
		break;

	    case 'l':
		lower = atof(optarg);
		break;

	    case 'S':
		sod_string = optarg;
		break;

	    case 't':
		trend = atof(optarg);
		break;

	    case 'U':
	    case 'h':
	    case '?':
	    default:
		printUsage();
		exit(1);
	}
    }
    if ( optind != argc )
    {
	printUsage();
	exit(1);
    }
    if ( ( when_string == NULL )
	 || ( in_specs->num == 0 )
	 || ( out_specs->num == 0 )
	 || ( countFileName == NULL ) )
    {
	printUsage();
	exit(1);
    }

    memset(&tm, 0, sizeof(struct tm));
    rc = sscanf(when_string, "%04u-%02u-%02u", &tm.tm_year, &tm.tm_mon, &tm.tm_mday);
    if ( rc != 3 )
    {
	fprintf(stderr, "Invalid date format\n");
	exit(1);
    }
    rc = sscanf(sod_string, "%02u:%02u", &tm.tm_hour, &tm.tm_min);
    if ( rc != 2 )
    {
	fprintf(stderr, "Invalid start of day format\n");
	exit(1);
    }
    tm.tm_mon--;
    tm.tm_year -= 1900;
    if ( mktime(&tm) == (time_t)-1 )
    {
	fprintf(stderr, "Can't process date\n");
	exit(1);
    }
    if ( tm.tm_isdst == 1 ) 
    {
	tm.tm_hour--;
    }
    sdate.tv_sec = mktime(&tm);
    sdate.tv_usec = 0;
    start_hour = (double)tm.tm_hour + (double)tm.tm_min / 60.0;
    tm.tm_mday += 1;
    if ( mktime(&tm) == (time_t)-1 )
    {
	fprintf(stderr, "Can't process date\n");
	exit(1);
    }
    fdate.tv_sec = mktime(&tm);
    fdate.tv_usec = 0;
    
    if ( finish < 0.0 )
    {
	finish = start;
    }

    numbins = 24 * 60 * 60 / binsize_sec;

    bin_b = 0 + (unsigned int)(2.0 * 60.0 * 60.0 / (double)binsize_sec);
    bin_c = numbins - (unsigned int)(2.0 * 60.0 * 60.0 / (double)binsize_sec);

    trend_per_bin = trend / (double)numbins;

    in_data = alloc_day_data(in_specs->num, numbins);
    if ( in_data == NULL )
    {
	fprintf(stderr, "Can't alloc data\n");
	exit(1);
    }
    out_data = alloc_day_data(out_specs->num, numbins);
    if ( out_data == NULL )
    {
	fprintf(stderr, "Can't alloc data\n");
	exit(1);
    }

    rc = read_multi_day_data(countFileName, in_specs, out_specs, sdate, fdate, binsize_sec, numbins, in_data, out_data);
    if ( rc != PM_RV_OK )
    {
	fprintf(stderr, "read_multi_day_data() returned %d\n", rc);
	exit(1);
    }

    printf("#Bin ");
    printf(" %18s", "Hrs since sod");
    for ( sidx = 0; sidx < in_specs->num; sidx++ )
    {
	sprintf(buf, "I%hu:%hhu:%hhu", in_specs->spec[sidx].sid, in_specs->spec[sidx].zid, in_specs->spec[sidx].cid);
	printf(" %10s", buf);
    }
    if ( in_specs->num > 1 )
    {
	printf(" %10s", "ITotal");
    }
    for ( sidx = 0; sidx < out_specs->num; sidx++ )
    {
	sprintf(buf, "O%hu:%hhu:%hhu", out_specs->spec[sidx].sid, out_specs->spec[sidx].zid, out_specs->spec[sidx].cid);
	printf(" %10s", buf);
    }
    if ( out_specs->num > 1 )
    {
	printf(" %10s", "OTotal");
    }
    printf(" %10s", "Population");
    printf(" %10s", "Corrected Pop");
    printf(" %10s", "Corrected Pop 2");
    printf(" %10s", "Error bound");
    printf(" %10s", "Corrected Pop 3");
    printf(" %10s", "Corrected Pop 4");
    printf(" %12s", "Lambda");
    printf(" %12s", "Alpha");
    printf("\n");
    for ( idx = 0; idx <= numbins; idx++ )
    {
	time_t t;
	struct tm * tmp;

	t = sdate.tv_sec + idx * binsize_sec;
	tmp = localtime(&t);

	printf("%02u:%02u:%02u", tmp->tm_hour, tmp->tm_min, tmp->tm_sec);
	printf(" %18.6f", start_hour + (double)(idx * binsize_sec) / 3600.0 );

	if ( idx == 0 )
	{
	    population = (int)start;
	    corrected_population = (int)start;
	    corrected_population2 = (int)start;
	    error_bound = ALPHA * start;
	    corrected_population3 = start;
	    corrected_population4 = start;
	}
	else
	{
	    population += (int)in_b - (int)out_b;
	    corrected_population += (int)in_b - (int)out_b;
	    corrected_population2 += (int)in_b - (int)out_b;
	    error_bound += BETA * (double)(in_b + out_b);
	    corrected_population3 += (double)(in_b) - (double)(out_b) - trend_per_bin;	    
	    corrected_population4 += (double)(in_b) - (double)(out_b) - trend_per_bin;	    
	}
	
	if ( idx < bin_b )
	{
	    upper_b = start + ( upper - start ) * (double)idx / (double)bin_b;
	    lower_b = start + ( lower - start ) * (double)idx / (double)bin_b;
	}
	else if ( idx < bin_c )
	{
	    upper_b = upper;
	    lower_b = lower;
	}
	else
	{
	    upper_b = finish + ( upper - finish ) * (double)( numbins - idx ) / (double)( numbins - bin_c );
	    lower_b = finish + ( lower - finish ) * (double)( numbins - idx ) / (double)( numbins - bin_c );
	}

	if ( (double)corrected_population < lower_b )
	{
	    corrected_population = (int)lower_b;
	}
	else if ( (double)corrected_population > upper_b )
	{
	    corrected_population = (int)upper_b;
	}

	if ( (double)corrected_population2 < lower )
	{
	    corrected_population2 = (int)lower;
	}
	else if ( (double)corrected_population2 > upper )
	{
	    corrected_population2 = (int)upper;
	}

	if ( corrected_population3 < lower )
	{
	    corrected_population3 = lower;
	}
	else if ( corrected_population3 > upper )
	{
	    corrected_population3 = upper;
	}

	if ( idx >= bin_c )
	{

	    if ( binsize_sec == 60 )
	    {
		beta = 0.8;
	    }
	    else if ( binsize_sec == 1 )
	    {
		beta = 0.97;
	    }
	    else
	    {
		if ( idx == bin_c )
		{
		    fprintf(stderr, "Warning: Don't have beta for binsize\n");
		}
		beta = 0.8;
	    }
	    lambda = -log(beta) / (double)( ( numbins - bin_c ) * ( numbins - bin_c ) );
	    alpha = beta + ( 1.0 - beta ) * exp( -lambda * ( idx - bin_c ) * ( idx - bin_c ) );
	    corrected_population4 = alpha * corrected_population4 + ( 1.0 - alpha ) * finish;
	}

	if ( corrected_population4 < lower )
	{
	    corrected_population4 = lower;
	}
	else if ( corrected_population4 > upper )
	{
	    corrected_population4 = upper;
	}

	in_b = 0;
	for ( sidx = 0; sidx < in_specs->num; sidx++ )
	{
	    if ( idx < numbins )
	    {
		printf(" %10u", in_data[sidx][idx]);
		in_b += in_data[sidx][idx];
	    }
	    else
	    {
		printf(" %10u", 0);
	    }
	}
	if ( in_specs->num > 1 )
	{
	    printf(" %10u", in_b);
	}
	out_b = 0;
	for ( sidx = 0; sidx < out_specs->num; sidx++ )
	{
	    if ( idx < numbins )
	    {
		printf(" %10u", out_data[sidx][idx]);
		out_b += out_data[sidx][idx];
	    }
	    else
	    {
		printf(" %10u", 0);
	    }
	}
	if ( out_specs->num > 1 )
	{
	    printf(" %10u", out_b);
	}
	printf(" %10d", population);
	printf(" %10d", corrected_population);
	printf(" %10d", corrected_population2);
	printf(" %12.1f", error_bound);
	printf(" %10.0f", corrected_population3);
	printf(" %10.0f", corrected_population4);

	if ( idx >= bin_c )
	{
	    printf(" %12g", lambda);
	    printf(" %12.8f", alpha);
	}
	else
	{
	    printf(" %12.8f", 0.0);
	    printf(" %12.8f", 0.0);
	}

	printf("\n");
    }
    printf("#Tot ");
    printf(" %18.6f", start_hour + 24.0);
    row_total = 0;
    for ( sidx = 0; sidx < in_specs->num; sidx++ )
    {
	col_total = 0;
	for ( idx = 0; idx < numbins; idx++ )
	{
	    col_total += in_data[sidx][idx];
	}
	printf(" %10u", col_total);
	row_total += col_total;
    }
    if ( in_specs->num > 1 )
    {
	printf(" %10u", row_total);
    }
    row_total = 0;
    for ( sidx = 0; sidx < out_specs->num; sidx++ )
    {
	col_total = 0;
	for ( idx = 0; idx < numbins; idx++ )
	{
	    col_total += out_data[sidx][idx];
	}
	printf(" %10u", col_total);
	row_total += col_total;
    }
    if ( out_specs->num > 1 )
    {
	printf(" %10u", row_total);
    }
    printf(" %10d", population);
    printf(" %10d", corrected_population);
    printf(" %10d", corrected_population2);
    printf(" %12.1f", error_bound);
    printf(" %10.0f", corrected_population3);
    printf(" %10.0f", corrected_population4);
    printf("\n");

    return 0;
}
